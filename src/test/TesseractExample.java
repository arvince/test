/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

/**
 *
 * @author Asus
 */
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;

import javax.imageio.ImageIO;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import java.io.File;
import net.sourceforge.tess4j.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
 
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class TesseractExample {
    
    //private static final String FILE_NAME = "C:\\Users\\Asus\\Desktop\\sample.xlsx";   
    
    public TesseractExample(String result, String path) {
        
        
        
            try {
            PrintWriter  writer = new PrintWriter(new FileWriter("E:\\ralph_sample\\samp.txt", true));
            
            String excelFilePath = path;
            //E:\ralph_sample
            FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
            Workbook workbook = WorkbookFactory.create(inputStream);
 
            Sheet sheet = workbook.getSheetAt(0);
 
            Object[][] bookData = {
                    {"", "", result},                    
            };
 
            int rowCount = sheet.getLastRowNum();
 
            for (Object[] aBook : bookData) {
                Row row = sheet.createRow(++rowCount);
 
                int columnCount = 0;
                 
                Cell cell = row.createCell(columnCount);
                cell.setCellValue(rowCount);
                 
                for (Object field : aBook) {
                    cell = row.createCell(++columnCount);
                    if (field instanceof String) {
                        cell.setCellValue((String) field);
                    } else if (field instanceof Integer) {
                        cell.setCellValue((Integer) field);
                    }
                    writer.write(field.toString());
                }
 
            }
 
            inputStream.close();
            writer.close();
 
            FileOutputStream outputStream = new FileOutputStream(path);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
             
        } catch (IOException | EncryptedDocumentException
                ex) {
            ex.printStackTrace();
        }
        
            
    }
    
    public static void main(String[] args) throws Exception, IOException{
            
//            //Loading an existing PDF document
//          File file = new File("FTD SET1 10-29.pdf");
//          PDDocument document = PDDocument.load(file);
//
//          //Instantiating the PDFRenderer class
//          PDFRenderer renderer = new PDFRenderer(document);
//          
//          for (int i = 2; i <= document.getNumberOfPages(); i++) {
//
//          //Rendering an image from the PDF document
//          BufferedImage image = renderer.renderImageWithDPI(i-1, 700);
//          
//          int x = 550;
//          
//          int y = 350;
//          
//          int w = image.getWidth() - 500;
//          
//          int h = image.getHeight() - 1000;
//          
//          BufferedImage out = image.getSubimage(x, y, w-x, h-y);          
//
//          //Writing the image to a file
//          
//          ImageIO.write(out, "JPEG", new File("sample 3.jpeg"));
//            
//
//         // System.out.println("Image created");
//
//          //Closing the document
//          
//
//            // System.setProperty("jna.library.path", "32".equals(System.getProperty("sun.arch.data.model")) ? "lib/win32-x86" : "lib/win32-x86-64");
//            File imageFile = new File("sample 2.jpeg");
//            ITesseract instance = new Tesseract();  // JNA Interface Mapping
//            // ITesseract instance = new Tesseract1(); // JNA Direct Mapping
//            // File tessDataFolder = LoadLibs.extractTessResources("tessdata"); // Maven build bundles English data
//            // instance.setDatapath(tessDataFolder.getPath());
//
//            try {
//                String result = instance.doOCR(imageFile);
//                //String result = instance.doOCR(image);
//                System.out.println(result);
//                TesseractExample example = new TesseractExample(result);
//            } catch (TesseractException e) {
//                System.err.println(e.getMessage());
//            }
//          }
//          document.close();
        /*
        try {

            FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();

            while (iterator.hasNext()) {

                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();

                while (cellIterator.hasNext()) {

                    Cell currentCell = cellIterator.next();
                    //getCellTypeEnum shown as deprecated for version 3.15
                    //getCellTypeEnum ill be renamed to getCellType starting from version 4.0
                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
                        System.out.print(currentCell.getStringCellValue() + "--");
                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
                        System.out.print(currentCell.getNumericCellValue() + "--");
                    }

                }
                System.out.println();

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Java Books");
         
        Object[][] bookData = {
                {"Head First Java", "Kathy Serria", 79},
                {"Effective Java", "Joshua Bloch", 36},
                {"Clean Code", "Robert martin", 42},
                {"Thinking in Java", "Bruce Eckel", 35},
        };
 
        int rowCount = 0;
         
        for (Object[] aBook : bookData) {
            Row row = sheet.createRow(++rowCount);
             
            int columnCount = 0;
             
            for (Object field : aBook) {
                Cell cell = row.createCell(++columnCount);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                }
            }
             
        }
         
         
        try (FileOutputStream outputStream = new FileOutputStream("JavaBooks.xlsx")) {
            workbook.write(outputStream);
        }
        */
         
        
        

    }    
}
   
